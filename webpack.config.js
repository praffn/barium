const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const normalize = require('node-normalize-scss').includePaths;
const webpack = require('webpack');

console.log(normalize);

module.exports = {

  context: path.join(__dirname, 'app'),

  entry: './index.js',

  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js'
  },

  module: {
    loaders: [
      // transpile es6 to es5
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel',
        query: {
          presets: 'es2015'
        }
      },
      // load html to strings
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      // compile sass and load css
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader!sass-loader')
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader',
        query: {
          removeSVGTagAttrs: false
        }
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      template: './index.ejs'
    }),
    new ExtractTextPlugin('app.css'),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en-gb/)
  ],

  postcss: {
    defaults: [autoprefixer]
  },

  sassLoader: {
    includePaths: [normalize]
  }

}
