import angular from 'angular'

import trustAs from './trustAs';
import removeLinks from './removeLinks';
import prettyDate from './prettyDate';
import timeAgo from './timeAgo';

const moduleName = 'app.filters';

angular.module(moduleName, [])
  .filter('trustAs', trustAs)
  .filter('removeLinks', removeLinks)
  .filter('prettyDate', prettyDate)
  .filter('timeAgo', timeAgo);

export default moduleName;
