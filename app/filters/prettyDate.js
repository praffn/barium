import moment from 'moment';

function prettyDate () {
  return function (input) {
    return moment(input).format('dddd, [the] Do [of] MMMM');
  }
}

export default prettyDate;
