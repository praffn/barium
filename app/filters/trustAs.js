trustAs.$inject = ['$sce'];
function trustAs ($sce) {
  return (input, type) => {
    if (typeof input === 'string') {
      return $sce.trustAs(type || 'html', input);
    }
    return '';
  };
}

export default trustAs
