import moment from 'moment';

timeAgo.$inject = [];
function timeAgo () {
  return function (input) {
    return moment(input).fromNow();
  }
}

export default timeAgo;
