function removeLinks () {
  return (input, str) => {
    let d = document.createElement('div');
    d.innerHTML = input;
    let links = d.querySelectorAll('a');
    for (let i = 0, len = links.length; i < len; i++) {
      links[i].remove();
    }
    return d.innerHTML;
  }
}

export default removeLinks;
