import tplHeader from './header.html';

HeaderController.$inject = ['$state'];
function HeaderController($state) {
  this.isHome = () => $state.current.name === 'home';
}

export default {
  template: tplHeader,
  controller: HeaderController
};
