import angular from 'angular';

import header from './header';

angular.module('app.components', [])
  .component('bariumHeader', header)

export default 'app.components';
