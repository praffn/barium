import home from './home.html';
exports.home = home;

import news from './news.html';
exports.news = news;

import calendar from './calendar.html';
exports.calendar = calendar;

import menu from './menu.html';
exports.menu = menu;

import music from './music.html';
exports.music = music;

import musicList from './musicList.html';
exports.musicList = musicList;

import post from './post.html';
exports.post = post;
