const constants = {
  API_ENDPOINT: 'http://api.keafredagsbar.dk/wp-json/wp/v2',
  GOOGLE_CALENDAR_URL: 'https://www.googleapis.com/calendar/v3/calendars/praffn.dk_rfrfj4t9vbhbuqma9ue9qqmq08@group.calendar.google.com/events?key=AIzaSyCSUft1KsP4xfh8BNN2cPBtpDLxGeb7s30',
  PWB: 'barenersej'
};

export default constants;
