run.$inject = ['$rootScope', '$anchorScroll'];
function run ($rootScope, $anchorScroll) {
  $rootScope.$on('$viewContentLoaded', () => $anchorScroll('top'));
}

export default run;
