import templates from '../templates';

config.$inject = ['$stateProvider', '$urlRouterProvider'];
function config ($stateProvider, $urlRouterProvider) {

  $stateProvider

    .state('home', {
      url: '/',
      template: templates.home,
      controller: 'HomeController',
      controllerAs: 'home'
    })

    .state('news', {
      url: '/news',
      template: templates.news,
      controller: 'NewsController',
      controllerAs: 'news'
    })

    .state('calendar', {
      url: '/calendar',
      template: templates.calendar,
      controller: 'CalendarController',
      controllerAs: 'calendar'
    })

    .state('menu', {
      url: '/menu',
      template: templates.menu,
      controller: 'MenuController',
      controllerAs: 'menu'
    })

    .state('music', {
      url: '/music',
      template: templates.music,
      controller: 'MusicController',
      controllerAs: 'music'
    })

      .state('musicList', {
        url: '/musiclist',
        template: templates.musicList,
        controller: 'MusicListController',
        controllerAs: 'musicList'
      })

    .state('post', {
      url: '/post/:postId',
      template: templates.post,
      controller: 'PostController',
      controllerAs: 'post'
    });

  $urlRouterProvider.otherwise('/')
}

export default config;
