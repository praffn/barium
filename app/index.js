import style from './style/main.scss';

import angular from 'angular';
import app from './app';

angular.bootstrap(document, [app.name]);
