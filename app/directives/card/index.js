import moment from 'moment';
import { random } from 'lodash';

import tpl from './card.html';

function card () {
  return {
    scope: {
      item: '=',
      noImage: '='
    },
    template: tpl,
    controller: CardController,
  }
};

CardController.$inject = ['$scope'];
function CardController ($scope) {
  $scope.prettyDate = date => moment(date).calendar();
  $scope.random = random(1, 32);
}

export default card;
