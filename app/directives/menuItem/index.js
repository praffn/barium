import tpl from './menuItem.html';

menuItem.$inject = [];
function menuItem () {
  return {
    template: tpl,
    replace: true,
    scope: {
      item: '='
    }
  };
}

export default menuItem;
