import tpl from './cardList.html';

function cardList () {
  return {
    scope: {
      items: '='
    },
    template: tpl
  };
}

export default cardList;
