import moment from 'moment';

import tpl from './event.html';

event.$inject = [];
function event () {
  return {
    scope: {
      event: '='
    },
    template: tpl,
    replace: true
  };
}

export default event;
