import tpl from './hero.html';

function hero () {
  return {
    template: tpl,
    link: function (scope, ele, attrs) {
      let heroContainer = ele[0].querySelector('.hero');
      let blurEle = heroContainer.querySelector('.hero__background--blurred');
      let logo = heroContainer.querySelector('.hero__logo');

      function update () {
        let imgHeight = ele[0].querySelector('.hero').offsetHeight;
        let y = window.pageYOffset;
        let percentageOfHeight = (100 / imgHeight) * y;
        let opacity = (percentageOfHeight / 100) * 2;
        blurEle.style.opacity = opacity;

        logo.style.bottom = `${percentageOfHeight / 2}px`;
      }

      function handleScroll () {
        window.requestAnimationFrame(update);
      }

      window.addEventListener('scroll', handleScroll);
      handleScroll();

      scope.$on('$destroy', destroy);

      function destroy () {
        window.removeEventListener('scroll', handleScroll);
      }
    }
  }
};

export default hero;
