import tpl from './back.html';

back.$inject = ['$window'];
function back ($window) {
  return {
    template: tpl,
    replace: true,
    link: function (scope, ele, attrs) {
      ele.on('click', () => $window.history.back());
    }
  };
}

export default back;
