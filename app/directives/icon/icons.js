import calendar from '../../assets/icons/calendar.svg';
import chevronLeft from '../../assets/icons/chevron-left.svg';
import comment from '../../assets/icons/comment.svg';
import crossCircle from '../../assets/icons/cross-circle.svg';
import home from '../../assets/icons/home.svg';
import menu from '../../assets/icons/menu.svg';
import music from '../../assets/icons/music.svg';
import news from '../../assets/icons/news.svg';
import search from '../../assets/icons/search.svg';

import keaSquircle from '../../assets/misc/kea-logo-squircle.svg';

export default {
  calendar,
  chevronLeft,
  comment,
  crossCircle,
  home,
  menu,
  music,
  news,
  search,

  keaSquircle
};
