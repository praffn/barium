import tpl from './icon.html';
import icons from './icons';

icon.$inject = [];
function icon () {
  return {
    scope: {
      icon: '@'
    },
    template: tpl,
    replace: true,
    controller: iconController,
    controllerAs: 'iconc'
  };
}

iconController.$inject = ['$scope']
function iconController ($scope) {
  this.svg = icons[$scope.icon];
}


export default icon;
