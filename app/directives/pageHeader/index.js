import { random } from 'lodash';

import tpl from './pageHeader.html';

const variants = 5;

pageHeader.$inject = [];
function pageHeader () {
  return {
    template: tpl,
    scope: {
      title: '@',
      variant: '@'
    },
    controller: PageHeaderController,
    controllerAs: 'pageHeader'
  }
}

PageHeaderController.$inject = ['$scope'];
function PageHeaderController ($scope) {
  this.variant = 'page-header--variant-' + ($scope.variant ? $scope.variant : random(1, variants));
}

export default pageHeader;
