iosDetect.$inject = ['$window'];
function iosDetect ($window) {
  return {
    link: function (scope, ele, attrs) {
      let isStandalone = ( ('standalone' in $window.navigator) && $window.navigator.standalone);
      if (isStandalone) {
        ele.addClass('standalone');
      } else {
        ele.addClass('not-standalone');
      }
    }
  }
}

export default iosDetect;
