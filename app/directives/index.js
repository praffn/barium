const moduleName = 'app.directives';

import angular from 'angular';

import hero from './hero';
import card from './card';
import cardList from './cardList';
import footer from './footer';
import back from './back';
import iosDetect from './ios-detect';
import pageHeader from './pageHeader';
import icon from './icon';
import event from './event';
import menuItem from './menuItem';

angular.module(moduleName, [])
  .directive('bariumHero', hero)
  .directive('bariumCard', card)
  .directive('bariumCardList', cardList)
  .directive('bariumFooter', footer)
  .directive('bariumBack', back)
  .directive('iosDetect', iosDetect)
  .directive('bariumPageHeader', pageHeader)
  .directive('bariumIcon', icon)
  .directive('bariumEvent', event)
  .directive('bariumMenuItem', menuItem)

export default moduleName;
