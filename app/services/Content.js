Content.$inject = ['$http', 'constants'];
function Content ($http, constants) {

  const API_ENDPOINT = constants.API_ENDPOINT

  function getLatest () {
    return $http.get(API_ENDPOINT + '/posts').then(response => response.data);
  }

  function getFrontpage () {
    return $http.get(API_ENDPOINT + '/posts/?filter[tag]=front').then(response => response.data);
  }

  function getPost (postId) {
    return $http.get(API_ENDPOINT + '/posts/' + postId).then(response => response.data);
  }

  return {
    getLatest,
    getFrontpage,
    getPost
  };
}

export default Content;
