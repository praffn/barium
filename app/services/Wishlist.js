Wishlist.$inject = ['Firebase', '$q'];
function Wishlist (Firebase, $q) {

  // const wishRef = Firebase.ref('wishlist');
  const timeoutTime = 10 /* minutes */ * 60 * 1000;

  function wish (item) {
    var deferred = $q.defer();
    let itemRef = Firebase.ref(`wishlist/${item.uri}`);
    let ji = item;
    // wishRef.push(item).then(deferred.resolve()).catch(deferred.reject());
    console.log(item.uri);
    itemRef.transaction(current => {
      if (!current) {
        ji.requests = 1;
        ji.created = new Date().getTime();
        return ji;
      } else {
        current.requests++;
        return current;
      }
    }).then(_ => deferred.resolve()).catch(_ => deferred.reject());
    return deferred.promise;
  }

  function resetTimeout () {
    window.localStorage.setItem('barium-wishlist-timeout', Date.now());
  }

  function canWish () {

    // remove
    return true;

    let timeout = window.localStorage.getItem('barium-wishlist-timeout');
    if (!timeout) return true;
    let currentTime = Date.now();
    let delta = currentTime - timeoutTime;

    return delta >= timeout;
  }

  function removeItem (key) {
    let deferred = $q.defer();
    Firebase.ref(`wishlist/${key}`).remove().then(_ => deferred.resolve()).catch(_ => deferred.reject());
    return deferred.promise;
  }

  return {
    wish,
    removeItem,
    resetTimeout,
    canWish,
  };
}

export default Wishlist;
