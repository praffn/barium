import angular from 'angular';

import Firebase from './Firebase';
import Wishlist from './Wishlist';
import Spotify from './Spotify';
import Content from './Content';
import Menu from './Menu';
import Calendar from './Calendar';

const moduleName = 'app.services';

angular.module(moduleName, [])
  .factory('Firebase', Firebase)
  .factory('Spotify', Spotify)
  .factory('Wishlist', Wishlist)
  .factory('Content', Content)
  .factory('Menu', Menu)
  .factory('Calendar', Calendar);

export default moduleName;
