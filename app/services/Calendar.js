import angular from 'angular';

Calendar.$inject = ['$http', '$q', 'constants'];
function Calendar ($http, $q, constants) {

  let self = this;

  function filterEvents (dates) {
    let today = Date.now();
    return dates.filter(event => {
      let eventDate = new Date(event.start.dateTime);
      return eventDate > today;
    });
  }

  function getEvents () {
    let deferred = $q.defer();

    if (self.events) {
      deferred.resolve(self.events)
    } else {
      $http.get(constants.GOOGLE_CALENDAR_URL).then(response => {
        self.events = response.data.items;
        deferred.resolve(self.events)
      });
    }

    return deferred.promise;
  }

  function getNextEvent () {
    let deferred = $q.defer();

    getEvents().then(events => {

      let today = Date.now();
      let record = Infinity;
      let winner;

      let happening = filterEvents(events);

      happening.forEach(event => {
        let eventDate = new Date(event.start.dateTime);

        if (eventDate - today < record) {
          record = eventDate;
          winner = event;
        }
      });

      deferred.resolve(winner);

    });

    return deferred.promise;

  }

  function getNextEvents () {
    let deferred = $q.defer();

    getEvents().then(events => {
      let happening = filterEvents(events);
      let sorted = happening.sort((a, b) => {
        let aDate = new Date(a.start.dateTime);
        let bDate = new Date(b.start.dateTime);
        return aDate > bDate;
      });
      deferred.resolve(sorted);
    });

    return deferred.promise;
  }

  return {
    getEvents,
    getNextEvent,
    getNextEvents
  };
}

export default Calendar;
