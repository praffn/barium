Menu.$inject = ['$http', 'constants'];
function Menu ($http, constants) {

  const API_ENDPOINT = constants.API_ENDPOINT;

  function getMenu() {
    return $http.get(API_ENDPOINT + '/barium_products', {
      params: {
        per_page: 30
      }
    }).then(response => response.data);
  }

  return {
    getMenu
  };
}

export default Menu;
