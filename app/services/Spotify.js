const SPOTIFY_CLIENT_ID = '68d1c4f62a3d4de3b21b294589c8dba2';
const SPOTIFY_ROOT_URL = 'https://api.spotify.com/v1';

function createEndpoint (endpoint) {
  return SPOTIFY_ROOT_URL + endpoint;
}

Spotify.$inject = ['$http', '$q'];
function Spotify ($http, $q) {

  function search (term) {
    // let deferred = $q.defer();
    return $http.get(createEndpoint('/search'), {
      params: {
        q: term,
        type: 'track'
      }
    }).then(response => {
      return response.data.tracks.items
    });
    // return deferred.promise;
  }


  return {
    search
  };
}

export default Spotify;
