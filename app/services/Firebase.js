import firebase from 'firebase';

Firebase.$inject = [];
function Firebase () {
  firebase.initializeApp({
    apiKey: 'AIzaSyBnKNth2-K6yC3MHuO3VBBUzWcqhXlZWBY',
    authDomain: 'keafredagsbar-550be.firebaseapp.com',
    databaseURL: 'https://keafredagsbar-550be.firebaseio.com',
    storageBucket: ''
  });
  return firebase.database();
}

export default Firebase;
