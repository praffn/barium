import angular from 'angular';

import components from './components';
import directives from './directives';
import controllers from './controllers';
import services from './services';
import filters from './filters';

import routes from './config/routes';
import run from './config/run';
import constants from './config/constants';

import uiRouter from 'angular-ui-router';
import typeahead from 'angular-ui-bootstrap/src/typeahead/index-nocss.js';
import animate from 'angular-animate';

const dependencies = [
  components,
  directives,
  controllers,
  services,
  filters,

  uiRouter,
  typeahead,
  animate
];

export default angular.module('app', dependencies)
  .component('app', {
    template: '<div class="app"><barium-header></barium-header><ui-view></ui-view></div>'
  })
  .config(routes)
  .constant('constants', constants)
  .run(run);
