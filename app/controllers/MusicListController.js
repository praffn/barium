import angular from 'angular';

MusicListController.$inject = ['Wishlist', 'Firebase', '$scope', '$timeout'];
function MusicListController (Wishlist, Firebase, $scope, $timeout) {
  let self = this;

  const wishRef = Firebase.ref('wishlist');
  wishRef.on('value', updateWishlist);

  function updateWishlist (snapshot) {
    let tmp = [];
    angular.forEach(snapshot.val(), item => {
      tmp = tmp.concat([item])
    });

    $timeout(_ => {
      self.wishlist = tmp.sort((a, b) => a.created < b.created);
    });

    // self.wishlist = tmp.sort((a, b) => a.created < b.created);
    //
    // console.log(self.wishlist);
    // // $timeout($scope.$apply(), 1);
  }

  self.removeItem = Wishlist.removeItem;

  // clean up on $destroy
  $scope.$on('$destroy', () => {
    wishRef.off('value', updateWishlist);
  });

}

export default MusicListController;
