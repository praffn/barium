import { extend } from 'angular';

PostController.$inject = ['Content', '$stateParams'];
function PostController (Content, $stateParams) {
  Content.getPost($stateParams.postId).then(d => extend(this, d));
}

export default PostController;
