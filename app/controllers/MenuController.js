import angular from 'angular';
import { shuffle } from 'lodash';

MenuController.$inject = ['Menu'];
function MenuController (Menu) {
  let self = this;
  self.menu = {};
  self.filterValue = 'All';
  self.categories = ['All'];

  Menu.getMenu().then(constructMenu);

  function constructMenu (menuItems) {
    self.items = shuffle(menuItems);
    angular.forEach(menuItems.sort((a, b) => a.acf.category > b.acf.category), item => {
      if (self.categories.indexOf(item.acf.category) === -1) {
        self.categories = self.categories.concat([item.acf.category]);
      }
    })
  }

  self.filter = function () {
    if (self.filterValue === 'All') return self.items;
    return self.items.filter(item => item.acf.category === self.filterValue);
  }

  self.filterBy = function (category) {
    self.filterValue = category;
  }

  self.filterIsActive = function (category) {
    return self.filterValue === category;
  }

  // function constructMenu (menuItems) {
  //   angular.forEach(menuItems, item => {
  //     let category = item.acf.category;
  //     if (!self.menu[category]) self.menu[category] = [];
  //     self.menu[category] = self.menu[category].concat([item]);
  //   })
  // }
}

export default MenuController;
