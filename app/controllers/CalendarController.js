CalendarController.$inject = ['Calendar'];
function CalendarController (Calendar) {
  Calendar.getNextEvents().then(events => this.events = events);
}

export default CalendarController;
