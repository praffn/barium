HomeController.$inject = ['Content', 'Calendar'];
function HomeController (Content, Calendar) {
  Content.getFrontpage().then(response => this.frontpage = response);
  Calendar.getNextEvent().then(response => this.nextEvent = response);
}

export default HomeController;
