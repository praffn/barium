NewsController.$inject = ['Content'];
function NewsController (Content) {
  Content.getLatest().then(d => this.posts = d);
}

export default NewsController;
