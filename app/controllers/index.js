import angular from 'angular';

import HomeController from './HomeController';
import MusicController from './MusicController';
import MusicListController from './MusicListController';
import MenuController from './MenuController';
import PostController from './PostController';
import NewsController from './NewsController';
import CalendarController from './CalendarController';

const moduleName = 'app.controllers';

angular.module(moduleName, [])
  .controller('HomeController', HomeController)
  .controller('MusicController', MusicController)
  .controller('MusicListController', MusicListController)
  .controller('MenuController', MenuController)
  .controller('PostController', PostController)
  .controller('NewsController', NewsController)
  .controller('CalendarController', CalendarController);

export default moduleName;
