import { debounce } from 'lodash';

MusicController.$inject = ['Spotify', 'Wishlist', '$state', 'constants'];
function MusicController (Spotify, Wishlist, $state, constants) {
  let searchSpotify = debounce(Spotify.search, 150);
  this.search = function (input) {
    if (input === constants.PWB) {
      $state.go('musicList');
    } else {
      return searchSpotify(input);
    }
  }
  this.onSelect = function (item) {
    this.selection = item;
  };
  this.wish = item => {
    Wishlist.wish({
      track: item.name,
      artist: item.artists[0].name,
      album: item.album.name,
      uri: item.uri
    }).then(Wishlist.resetTimeout)
  };

  this.canWish = Wishlist.canWish;

  Spotify.search('sorry').then(d => console.log(d));

}

export default MusicController;
