var webpack = require('webpack');
var config = require('./webpack.config');

config.devtool = 'source-map';

// easy file loading
config.module.loaders.push({
  test: /\.(gif|png|jpg|jpeg)$/,
  loaders: ['file']
});

module.exports = config;
