var webpack = require('webpack');
var config = require('./webpack.config');

config.plugins.push(
  new webpack.DefinePlugin({
    'process.env': {
      'NODE_ENV': JSON.stringify('production')
    }
  })
);

// Add minifier/uglifier
config.plugins.push(
  new webpack.optimize.UglifyJsPlugin()
);

config.devtool = 'source-map';

// optimize images
config.module.loaders.push({
  test: /\.(gif|png|jpg|jpeg)$/,
  loaders: [
    'file',
    'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
  ]
});

module.exports = config;
